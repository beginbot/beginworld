# Ladka8

ladka8: I got my first internship begin, React native dev at a med tech startup
ladka8: 6months

## Internship Concepts

- It's NOT About output
- It's about experience, learning, growth, development, networking etc.
- Attitude is like 90% what I'm going to base how much I give back to
  an intern. We could pair all day, teach you soo much stuff.......
  if you're not a jerk and annoying to be around.

## Extra Intern Challenge

- Intern in WFH times
- you lose the lunchroom hangouts, the run into random people at the coffee spot.
- How are you going to going to get to know people?

## Fears

ctnight: I get the importance of going off and experimenting on your own, but in the context of an internship isn't there the danger of wasting time and going down unnecessary dead end roads if you don't avail yourself of the expertise around you?

Myself and my coworkers, DO NOT DEPEND ON ANY WORK FROM INTERNS
.....we are not hinging the company on interns getting things done.
I DONT CARE
WE DONT CARE
YOU'RE OUTPUT COULD NOTHING
IT COULD ALL BE JUST EXPERIMENTS

....if a company hires you, and its all just work, and
no mentorship, and no pay, thats an internship, thats a trick.

What we do care about:

- The intern learning
- The intern getting experience
- The intern trying things out, we can't try out,
  because we have deadlines

Make sure you are interacting with the company and multiple employees
often. Don't find yourself just coding all alone in a room.

## Good Intern Versus Bad Intern

In my experience, you don't have all the work
planend for an intern, Good interns, find
where/how to helpful and just take
initiate and do it.

Be cognizant of developers time, and
make sure when you're asking for help,
you have a good idea what you need help on.
Notes are useful.

## The Dream Intern Interaction

Hey Begin, I was working on X project, that would help us in Y way.
However I'm having trouble understanding/or hooking up Z to J.

I did some reserach and heres what I found.
I tried this, and saw this.

Could you help me understand what I'm missing.

- Notes
- Concrete Problem
- What you already tried
- You have the larger concept you are confused about outlined.

Dream Intern thinks like an employee. How can I help the company, or other
developers with my code.

## Worse Intern Interaction

- Hey I got a ticket that says do X
  How do I do X

.....uh what have you tried?

....nothing, just tell me how to do it

## The Middle of the Road Intern

- Will do whats told, won't look for/create their own opportunities.

## Questions for Ladka

- What are you goals??
- What are you excited about learning?
- What are you scared about?
- Who is mentoring you?
- Other interns?
- What does a successful internship look like?
  - Job offer?
  - Asked to return for another session?

## Other Intern Experiences

carlvandergeest: my best intern was the one that embraced devops and knew our
build/deploy system better than our senior devs that hate any and all change
carlvandergeest: yes we hired the hevk outta that guy

