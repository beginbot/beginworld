# Linode

- Buckets
  - Beginworld site finish
- Deploy Go application
  - Packer
    - https://www.packer.io/docs/builders/linode
    - https://github.com/hashicorp/packer/issues/174
- Deploy Database
  - Something from the market place? Nah
- Docker/Docker-Compose

## Basics

- put a random soundfile.txt in there,
  - and have it so when people

---

## Start the Scavenger Hunt

Beginworld is recruiting!
  - The Others
  - The other side of the game
  - Do you want to help hide things???

- Trick yall into learning about various ways of interacting with services
  Linode

Every 15 minutes in the chat:
  - Drop a Access Keys to a Bucket
  - The games began

Level 1: Object Storage Linode Bucket
  - Hide sounds, that if you find a play you win a prize
  - Hiding mean????
    - In a bucket
      - Drop temporary creds for people, to read from buckets
        (Note: we also want to allow others fill the buckets with Trash)
      - In the bucket there will be a Key (I think it'll me funny/fine/awesome)
        private keys

Level 2: Linode Instance
  - You got an SSH key, and SSHed up the server.
    - how do we hide the database
  - Another Path:
    - Have a bucket that users can write to public keys to,
      that we will pull in to the instance.
      - If people can figure out how to upload their SSH key
        get access

Level 3: Database
  - You find the creds and connects
  - Mystery for how your query the info, to find a secret/key/prize
    - What kind of queries should we force people to learn for this

### Why

.....You actually got some creds! ...now can you use them?
.....How can we make a game out of out

### The 3 Credentials we want to Leak:

  -> Linode Object Storage Access Keys
  -> Private SSH Key
  -> Database Credentials

WE need to track all credential creation and "deletion",
so we can identify how old the credentials people
are trying to use.
There is a difference, in someone using 1 month old or a 15 mins old

What if there is a sound in the Database on a Linode Instance,
that if you play on stream, you win that day!
!secret127373

### Things We'll Trick you into learning:

- SQL
  -> we fill give you some data, and a general puzzle
     of how to find the info. You'll have to construct
     a query to find the data you need (the data might be a sound you play)

- Git:
  -> Repo with the secret somewhere in it.
    -> git message
    -> git branch

- SSH
  - PGP Keys

- CLI Tools

- debugging and searching for data

- some infra

## View Concerns

falloutghst: @beginbot isn't the whole game prone to people already knowing some
/ all of the skills you'd like to teach them farming all the points?

beginbot: The Others, is for the people with these skills, who
  want to help be creative, or push their skills in interesting ways

We want to reward people, for how far they got:
  - 4 Levels:
    - on the bucket
    - on the instance
    - in the DB
    - found the prize

What credentials are we going leak?
  - How can leak a user who can only query

We are going to continue automating the creation and destruction of the system.
Meaning: If we leaked all keys, and things got taken over. We need a simple
shut down, and simple rebuild

### The Others

- How can we hide Private SSH keys in a bucket
- What else should we fill the bucket up with
  - We should allow "The Others" write access

- How can we hide the Database password on the Linode instance,
  in an interesting way.

#### The Others Team

  - carlvandergeest: !linode
  - Honory Other: rexroof
    rexroof: images in scavenger-bucket should have secrets in their EXIF

### Is identical

- I would setup my compiling on of Rust on Linode
- Do on an instance

---

# Goal: Deploy a Linode Instance with Terraform and Packer and SSH onto it

## Packer Questions

What are communicators?
  - https://www.packer.io/docs/templates/communicator

## Steps

Visit Here: https://www.packer.io/docs/builders/linode
Copy the Example at the bottom into a `instance.json` file

```
{
  "type": "linode",
  "linode_token": "YOUR API TOKEN",
  "image": "linode/debian9",
  "region": "us-east",
  "instance_type": "g6-nanode-1",
  "instance_label": "temporary-linode-{{timestamp}}",

  "image_label": "private-image-{{timestamp}}",
  "image_description": "My Private Image",

  "ssh_username": "root"
}
```

Only the First 6 are required (I need ssh_username)
{
  "type": "linode",
  "linode_token": "YOUR API TOKEN",
  "image": "linode/debian9",
  "region": "us-east",
  "instance_type": "g6-nanode-1",
  "ssh_username": "root"
}
```


Not Required
```
{
  "instance_label": "temporary-linode-{{timestamp}}",
  "image_label": "private-image-{{timestamp}}",
  "image_description": "My Private Image",
  "ssh_username": "root"
}

## Minimal Example for Begin

```json
{
  "type": "linode",
  "linode_token": "YOUR API TOKEN",
  "image": "linode/arch",
  "region": "us-east",
  "instance_type": "g6-nanode-1",
  "ssh_username": "begin"
}
```

## Options

images: linode/debian9, linode/fedora28, linode/ubuntu18.04, linode/arch, and private/12345
region: us-east, us-central, us-west, ap-south, ca-east, ap-northeast, eu-central, and eu-west.
instance_type: g6-nanode-1, g6-standard-2, g6-highmem-16, and g6-dedicated-16.

## Environment Variables in Templates

https://www.packer.io/docs/templates/user-variables.html#environment-variables

> The env function is available only within the default value of a user variable,
> allowing you to default a user variable to an environment variable. An example
> is shown below:

```json
{
  "variables": {
    "linode_token": "{{env `LINODE_TOKEN`}}"
  },
  "builders": [{
    "type": "linode",
    "linode_token": "{{user `linode_token`}}",
    "image": "linode/arch",
    "region": "us-east",
    "instance_type": "g6-nanode-1"
    "ssh_username": "begin"
  ]}
}
```

## Packer Validate

```
packer validate image.json


```
ssh-keygen -f ~/.linode/.ssh/begintime

chmod 400 ~/.linode/.ssh/begintime
```

```
packer build
```

## Transferring From

isidentical: dedicated 8GB ram + 4 core xeon server for about 25$

## Linode Long Term

- We want to help people do stuff the cheapest

## Huge Win

- We just got a Huge Celebrity on Linode : isidentical
isidentical: It would be nice if there were a plan for 2 cpu / 2gb ram, 2 cpu / 4gb ram looks a bit overpriced but that is the lowest package with multiple cpus
rexroof: ssyuni is in a phone prison.

Why would you have 3 phones:
- One for the Load
- One for the Plug
- One for your Boo

isidentical: wait, why can't I deploy a server for multiple months? There is no choice for a server to how many months it should be claimed
