# 2020 - 8 - 14

## Go Syntax Sugar

- argument types
	) (<-chan interface{}, <-chan interface{}) {
	) (_, _ <-chan interface{}) {

## The most valuable comment

- is the one warning about non-obvious "sirens on the rocks of refactoring"

## Pairing on something

- we had a test;
  - we wrote 3 implementations
    - one was over the top OO
    - Super functional
    - Golfed

- in our code, whereever it reach this section, it just
choose a random branch......someone found this later

random.rand(func_to_call)

## Universe Truth of Programming

- Temporary Solutions are Final
  - Someone says lets start with a NoSQL, since we don't know 
    the structure yet, we'll evolve!
    ....nope

This will make us go faster now, and later we will make it "proper".
.....later doesn't exist.

## GO Learnings

What do Stages in Go Look Like:
  - done <-chan interface{} as first argument
  - used in range, can be nested

When is a Stage a good Fan-Out canidate:
  - Order isolation (can't be order dependent)
  - Its taking a while

How do you fan-in:
- One goroutine for each of the channels we are "fanning-in"
- One goroutine that waits for all the channels to be done, and says so

## Resources

baldclap: There a great series on youtube called Git for Poets
baldclap: goes over the basics like a blues clues episode
baldclap: https://www.youtube.com/watch?v=BCQHnlnPusY&vl=en

https://www.ijsr.net/archive/v9i4/SR20409190322.pdf
- https://www.boredpanda.com/funny-worst-input-fields/?utm_source=google&utm_medium=organic&utm_campaign=organic

## Experiences

- We did the first streaming of the podcast,
  and it was very weird for me to stream without
  constant SFXs. Once they were back I felt better.

isaiahvander: @beginbot got one rejection from my interviews last friday. the other hasn't gotten back to me but i'm assuming it's also a no. been applying all week and haven't heard much. been up for about an hour so far still doing applications.

isaiahvander advice: Write down what you wish you had know for each interview,
definitely reach out for feedback.

ladka8: begin if I wanna freelance in web development what sort of portfolio should I make? How complex do the projects need to be

What are you going to sell to people:
  - Do you have a shopify integration
  - Some sort of blog integration
  - Some sort of nice gallery of things

Not too large random projects, that demonstrate distinct things
you can build

## Freelancing Experience

I started a different amount/style of amount each gig.
- Kept experimenting

## A Simple Question

How do you sell your skills to someone else:

- Portfolio???
- Smooth talker
- Recommendation from someone
- Open Source contributions
- Blog Posts
- Youtube Videos

NOTHING IS REQUIRED
.....someone could literally just go:
"I like ya style kid, how about doing some work for me"

## Begin Obsession

- Can I do something "real", Create and Solve my Own problems,
not solve problems that are set out already


## What Difficulity did Everyone Choose for Programmer?

- You can choose easy
  - Be a nice, cool, fun person
  - Try and help out

- Prestige Mode / 00 Agent
  - Make no friends
  - You're a jerk to everyone

Hard == Good
Easy != Good

Nothing good comes easy

## Ladka Advice

- Apply for a job on Upwork
- Start a project you intend to show to get work, and choose a deadline, start
  that project!!!!!
