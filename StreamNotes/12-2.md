# 12-2.md

## What we need to DO

- buying packs
- listing packs

!listpacks

----

Interesting Things

!milkwifetits
!melkman
!ussr
!primeagen
!teejblur
!teej
!teej1
!teej2
!zoom primeagen
!spin primeagen
!melkman2
!melkman
!roxkstar1
!norm primeagen
!spin primeagen
!spin melkman
!zoom roxkstar

!norm roxkstar
!norm melkman
!pourthemilk

!zoom popupteej
```
func NewDuplicateSceneItemRequest(
```

## Goofiness Next

- A more dynamic Way to trigger effects on "Source", "SceneItems"

!zoom Screen
!zoom 922
!teej1
!zoom teejpopup
!zoom primeagen
!zoom2 teejpopup
!spin teejpopup
!spin primeagen
!primeagen

## Secret of Programming

....The learning phase never ends

## Fundamental Question

- Which comes first, the OBS effects or the sounds?
  -> effects produce sounds
  .....or sounds produce effects

  ...I think sounds should be first (since they are more likely to be queued)

## Link between Sounds and Effects

- Certain sounds -> Should trigger certain effects
  -> This means that the soundplay portion
    needs produce the effects

## TODO

- Soundpacks
- OBS effects
- TODO streamlord preview command
  -> hook up the requests site generation

## Features we losing temp

- widebegin
- rise

```
audioRequest, err := soundboard.CreateAudioRequest(db, "wideputin", "beginbotbot")
if err != nil {
  fmt.Printf("err = %+v\n", err)
  continue Loop
}
audioRequests <- *audioRequest

audioRequest, err := soundboard.CreateAudioRequest(db, "myfg", "beginbotbot")
if err != nil {
  fmt.Printf("err = %+v\n", err)
}
audioRequests <- *audioRequest
```
## Viewer Go Comments

Started on that go by example here's my likes/dislikes: 1) Only for loops, brilliant 2) Not many abstractions 3) Can we please lowercase the P in fmt.Println, it's like the only capital letter in the language


## Stream TODO

- Figure out Stupacs Nigthtbot Mod commands
- Add Streamgod to Audiorequest
