# 11-9

[3.619ms] [rows:0] SELECT * FROM "stream_commands" WHERE name = 'props @zanuss' ORDER BY "stream_commands"."id" LIMIT 1
        Searching for part = zanuss
        ParsedCommand: {props punchypenguin 0 zanuss zanuss 0}
@punchypenguin Gave @zanuss 1 props
parts: [!arch]
filename Found = arch.opus
        Attempting to save sound: soundeffectpanic: runtime error: index out of range [1] with length 1



## Viewer Questions

annacodes: what's the difference between a terminal and a terminal emulator and a virtual terminal emulator?

beginbot:

- back in the day, we had a terminal to interface with on central computer
...then we added a terminal emuluator, which is a computer


## Things to Ponder

chaelcodes: What would you use to generate bookmarks automatically though? Method names? That'd be hard to debug in long functions. Including the code would be a pain to debug too. then you have to think about debugging production environments and build systems. Code links are fine locally, but when you're moving between systems it breaks down.

endofunctorzero: @ChaelCodes ctags

I use custom Global Marks for semi-persistant bookmarks when refactoring
Debugging Prod: We have some logs, in a structured manner
some sort of what to stich to loves together: Open Telemetry
Line numbers do exists in Prod, production Stack - Traces

- Error line 22 on main.go

## Today

- Transfer more features from Python to Go
- Fix Bugs
- Learn Things
- Have Fun

## What's New

- !love / !hate
  -> the more you're loved, the more you can love
- !buy -> is working
  - is faster
  - random works
  - multiple amounts works
- !perms
- !props
- !soundeffect
- !me

## Philosophy Conundrums

- Time Doesn't Fly
  -> time exists as a thousand points of light in Matrix

## Secret Technique I Learned

- I'm a Dream It, Then Achieve

Right now, I'm dreaming of becoming of a Twitch Partner
....So I'll achieve hie that soon
then I have like 20+ new emotes
I'm going to sell right away!

## Confession

- When I was highschool -> Youth Large;
  -> if I went music shows, had to get those Youth Large fast, they went fast

... I watched Prime play a game
....I'm soo behind...I can't even understand whats going on
....will I ever catch up
FOMO - Fear of Missing Out

I thought of making a circiulumn: cirikkiliumiimim
  - To catch up on 20 years of gaming

....When I was first learning HTML...I told myself, I can't wait until I can
afford DreamWeaver and see whats thats like

....6 months later I saw wht it as....and it was funny

## Code Quality Conundrum

- Yes we need to care about Code Quality

...but you ever have someone who spends soo much time Gold-Plating
   that you have to finish their work and support it.
   ....And they just sit in some Ivory tower, make plans
   ....for the "Cleanest Code" and the most HexlogicalOrthogal Architecture
  .....you've ever heard of....but will never see

We need to balance helping our coworkers TODAY and helping them TOMORROW (or 6
months from now)

## 2 of the greatest but most unused features of being a programmer

- Time Independence
- Location Independence

Work when you want, Where you want.

Work in the morning, take a break, ride you're bike, read a book,
have some fun, work later. Who cares!

Don't want to go the office, fine! Go to work at Home,
or the Coffee shop
talk a vacation to another city and work there!

## TODO

- Try and understand
- https://www.wikiwand.com/en/L%27H%C3%B4pital%27s_rule
