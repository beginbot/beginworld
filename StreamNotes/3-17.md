# 3-17

## A Better Take

I want to say:

- $take iron-plate
  -> This should look in all inventory lots?
      inventory.furnace_result
      inventory.chest


We should need to find all of the entities around me,
and find which ones have inventory slots I'm interested in

## Current Issues

- $build stone-furnace
  not building consistently

- Better coaling everywhere

## Today

- Let Chat control more of Factorio
- Club Penguin


## Innovations

- you can pass an amount to directions

```
$north 4
$southwest 4
```

- Notifications from Factorio to Twitchchat

## Twitch Streamer Innovation

I've removed 90% of the words from the most common-boring troll:
  - Whats the word: imagine

