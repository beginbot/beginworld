# 10-15

## Only Holiday I Celebrate

- Sad Day:
   - planned day of sadness
   - nothing you like
   - can't sit in chairs (must sit on floor)
   - only one food you can: cold hotdogs
   - no stimulation
   - you can only sit and be sad
   - Music's ok

Benefits:
  - Next day you're sooo happy
  - Coffee tastes better
  - Sunshines nicer

I don't take sick days. I take off when I want to be off.
My theory: no one gets sick, people just want a day off.
so if I want a day off. I just say, I'm going to take the day off.
West Coast Startup Trick Land: unlimited PTO....which means no one takes off

## Prove me Wrong

- The most riots based around an instrument: Bassoon

## Which Better

- Homeless person hanging with their dog all day, chilling, petting em
- Rich fancy person, whose dog they only see an hour after work all day
  and the walker has taken them on a walk.

## Later Treat

soulshined: this is my favorite sax solo - just a treat for your personal begin time https://youtu.be/73r9AftpGbo

## Viewer Questions

anakimluke: begin, what is love?
beginbot:   ....is not wanting things from someone else, but wanting to help
them altruistically???

raiiyui: What can i learn besides programminglanguages to be a better softwaredeveloper?
beginbot: Softskills! How are your tickets? How are your notes from meetings?
         How are you 1-1s? What are the hard "soft" projects you're working on.
erikdotdev: @Raiiyui software design and architecture, operating systems, data
structures and algorithms, infrastructure


johnythecarrot: How did I use Linux for 8 months and not know about any of these cool magic things you're doing with your terminal
beginbot: When are you looking at your work flow, and asking yourself:
  - "I wish I could do X here"


stupac62: @beginbot how do you feel about calling some code "utils"? haha
beginbot: uneasy......whats worse:
   - a toolbox of unassociated functions
    - if you got a toolbox, and functions of similar abstraction start
      appearing, its as more natural abstraction point.
   - or incorrect abstraction

## The Linux and Windows

johnythecarrot: One big frustration I have that is probably not solvable is that I need some things that are Win exclusive and other things that are Linux exclusive and switching is incredibly annoying
soulshined: wsl @JohnyTheCarrot ? VM?
johnythecarrot: I'll prob use a VM after I switch to an SSD
beginbot: What are these "window Specific programs"???

## Today

- More Go

## Hot News

mrsommerfeld: see https://github.com/neovim/neovim/pull/12953

## Begin Internal Struggle

- should we switch from go-pg, to sqlx
  - go-pg -> that that Go-like. Idiomatic Go???
  - sqlx IS NOT AN ORM
  - I'm one reason away from switching
  - Work is using go-pg, but I ain't like it.
    - I will eventually match wor
  - Gorm for migrations

go-pg:
  - ORM
  - DB Driver
  - Migration support
OR:
  - pgx -> DB Driver
  - sqlx -> Kinda the ORM (not really, but he Marshal and prepared statements help)
    - Marshal rows into structs (with embedded struct support), maps, and slices
    - Named parameter support including prepared statements
    - Get and Select to go quickly from query to struct/slice
  - gorm -> migrations

  - https://github.com/gobuffaloio/pop

## Go Questions

- Pythonic
  - Gonic
  - Gyle
  - Gopheric
  - Goophfy
  - Gophonic***
  - Gophery??

  ***
  ****

What are these struct annotation things called, how do they work,
how do I make my own??

`tags`


```go
type User struct {
  name `db:username`
}
```


is go-pg: ORM and Sql Driver???

Just driver???
https://github.com/jackc/pgx

### Uses for Tags

- automatic unmarshaling of JSON into structs
  - often they are for translation
  - We are going to turn this thing into a struct, heres how to map the fields
  - or the reverse, we are going to use this struct as this other
    data-structure, map said fields.

