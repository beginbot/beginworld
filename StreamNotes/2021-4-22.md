# 2021-4-22

## Past as Gym Leader

me and 2 Friends had our own Gyms, themed appropriately:

- Pure Defense - Crowing Pokemon Level 100 Cloister
- One hit Kill Gym - All pokemon with one hit kills
- My Gym: Cutest Pokemon, wigglytuff, jiggly, clerairy, clefable, chansey


Title For the Level:
Supply Goals

```
{
  name = 'level-6',


  init = function()
    goals.set_goal("Level 6: New Frontier")
    goals.set_supply_goal({
      ["automation-science-pack"] = 10,
    })
    set_info({custom_function = goals.update_materials_gui})
  end,


  condition = function()
    if goals.has_completed_goals() then
      story_jump_to(global.story, 'level-7')
      return true
    end
  end,


  action = function()
    print("Running level 6 branch")
  end
}
```

## Actual Phrases I Like

- Make the change Easy, THEN make the easy change


....You get a feature:

"OH NO THATS IN THE MIDDLE OF BIG PILE SPAGETTI, SO MANY ANNOYING CORNER CASES"
...wish I could refactor

Let's make the change easy to complete, meaning we refactor the code base
to make more added the change simple.

Then add that simple change after the rector


---

The main skill between Junior and Senior:
  - Knowing when to Duct Tape, and when to Gold Plate

---


## Begin Factorio Challenge 1:

2 Teams with a Time Limit
  - Who collects the MOST Red Science:

- How long would it take a team to make any red-science?

What you need for Red-Sci:
  - iron-gear wheel
  - copper-plates

You can build craft red-science manually.

....do we want to allow that
.....

## Challenge 2

- First to Green Science

---




- Control Factorio

## Factorio

- Control Character Better
- Actually checking for Electricity

## Problems

- We need to test electricity, but thats too far in the game
  - How can we cheat easier????
    -> Generate the items we need, through Lua
      -> Water Pump, Steam Engine, etc.
    -> Get to a scenario, save and branch off from it
      -> Stop the game, copy the save file

- How do I have access to call functions in the lua-console
  inside of factorio?
  - Where do I have to load them, to make them accessible

## Factorio Theory

- When you create an entity, it ignores the restrictions, around where
  you can build.

  Example: offshore pumps can only be built in one orientation,
            ...unless you use create_entity

  How do you populate you're own inventory

## Begin Questions for Viewers


greatest_lord: @beginbot i've heard recently from some seniors devs who said that lua garbage collector is garbage and it causes 'catastrophes' if the project is big :}

- Whats a castastrophe?
- What is "garbage" about it?
- What constitutes big?


## Stream Life

- You always improve after each major event/failure on stream
  - First raid == lots of improvements the next day


## Go as a First Language


- You don't have to worry about styling
- There isn't a huge amount of syntax to learn
- The "boiler-plate" is good for newer programmers
  to get reps in writing common statements
- Lots of ways to build something that does something fast


## Begin Theory

- If you needed to pay all your bills with programming, starting from 0:
  - most people will be able to get there MUCH FASTER, with Ruby over C

....The best first lang, is the one you like programming most,
and you will keep programming until you find another lang.






@Coderish_64 "At 264 million, rural India registered an internet growth at 45 per cent while **urban** India logged 11 per cent as overall monthly active internet usage crossed 574 million user base in 2019 which is an annual growth of 24 per cent, according to the report titled ‘ICUBETM 2019’." https://www.expresscomputer.in/industries/education/38-school-going-kids-have-access-to-internet-in-india-report/55073/



can_be_destroyed() → boolean	Checks if the entity can be destroyed
destroy(opts) → boolean	Destroys the entity.
has_command() → boolean	Has this unit been assigned a command?
die(force, cause) → boolean	Immediately kills the entity.
has_flag(flag) → boolean	Test whether this entity's prototype has a flag set.
ghost_has_flag(flag) → boolean	Same as LuaEntity::has_flag but targets the inner entity on a entity ghost.
remove_market_item(offer) → boolean	Remove an offer from a market.
connect_neighbour(target) → boolean	Connect two devices with wire or cable.
order_deconstruction(force, player) → boolean	Sets the entity to be deconstructed by construction robots.
to_be_deconstructed() → boolean	Is this entity marked for deconstruction?
order_upgrade{force=…, target=…, player=…, direction=…} → boolean	Sets the entity to be upgraded by construction robots.
cancel_upgrade(force, player) → boolean	Cancels upgrade if it is scheduled, does nothing otherwise.
to_be_upgraded() → boolean	Is this entity marked for upgrade?
set_request_slot(request, slot) → boolean	Set a logistic requester slot.
is_crafting() → boolean	
is_opened() → boolean	
is_opening() → boolean	
is_closed() → boolean	
is_closing() → boolean	
launch_rocket() → boolean	
supports_backer_name() → boolean	
play_note(instrument, note) → boolean	Plays a note with the given instrument and note.
connect_rolling_stock(direction) → boolean	Connects the rolling stock in the given direction.
disconnect_rolling_stock(direction) → boolean	Tries to disconnect this rolling stock in the given direction.
rotate(options) → boolean	Rotates this entity as if the player rotated it
is_connected_to_electric_network() → boolean	Returns true if this entity is connected to an electric network.
can_shoot(target, position) → boolean	If this character can shoot the given entity or position.
mine(options) → boolean	Mines this entity.
can_wires_reach(entity) → boolean	Can wires reach between these entities.
is_registered_for_construction() → boolean	Is this entity or tile ghost or item request proxy registered for construction?
is_registered_for_deconstruction(force) → boolean	Is this entity registered for deconstruction with this force?
is_registered_for_upgrade() → boolean	Is this entity registered for upgrade?
is_registered_for_repair() → boolean	Is this entity registered for repair?
active :: boolean [RW]	Deactivating an entity will stop all its operations (car will stop moving, inserters will stop working, fish will stop moving etc).
destructible :: boolean [RW]	When the entity is not destructible it can't be damaged.
minable :: boolean [RW]	
rotatable :: boolean [RW]	When entity is not to be rotatable (inserter, transport belt etc), it can't be rotated by player using the R key.
operable :: boolean [RW]	Player can't open gui of this entity and he can't quick insert/input stuff in to the entity when it is not operable.
supports_direction :: boolean [R]	Whether the entity has direction.
driver_is_gunner :: boolean [RW]	Whether the driver of this car or spidertron is the gunner, if false, the passenger is the gunner.
to_be_looted :: boolean [RW]	Will this entity be picked up automatically when the player walks over it?
power_switch_state :: boolean [RW]	The state of this power switch.
remove_unfiltered_items :: boolean [RW]	If items not included in this infinity container filters should be removed from the container.
armed :: boolean [R]	If this land mine is armed.
recipe_locked :: boolean [RW]	When locked; the recipe in this assembling machine can't be changed by the player.
enable_logistics_while_moving :: boolean [RW]	If equipment grid logistics are enabled while this vehicle is moving.
moving :: boolean [R]	Returns true if this unit is moving.
allow_dispatching_robots :: boolean [RW]	Whether this character's personal roboports are allowed to dispatch robots.
auto_launch :: boolean [RW]	Whether this rocket silo automatically launches the rocket when cargo is inserted.
request_from_buffers :: boolean [RW]	Whether this requester chest is set to also request from buffer chests.
corpse_expires :: boolean [RW]	Whether this corpse will ever fade away.
corpse_immune_to_entity_placement :: boolean [RW]	If true, corpse won't be destroyed when entities are placed over it.
is_entity_with_force :: boolean [R]	If this entity is EntityWithForce
is_entity_with_owner :: boolean [R]	If this entity is EntityWithOwner
is_entity_with_health :: boolean [R]	If this entity is EntityWithHealth
valid :: boolean [R]	Is this object valid?






