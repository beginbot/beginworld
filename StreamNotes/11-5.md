# 11-5

## Culture Barrier

In America Bigger is Better
Saying someone is fat
is saying someone is better

Formal Goal: 200 by the end of the year

---

!soundeffect https://www.youtube.com/watch?v=wfzadSG4NH0 puppy

!soundeffect https://www.youtube.com/watch?v=wfzadSG4NH0 puppy 00:00 00:05

!soundeffect https://youtu.be/pmaoMVPk76o sadeyedlady 00:26 00:33

## Current Issue

tx.Error = cannot convert {https   www.youtube.com /watch  false v=wfzadSG4NH0  } to Text:

Write now, we are relying on GORM, to take the URL and convert to a "string"
"text", it's failing

What could we do?
  - Can we tell GORM?
  - I could update the struct to always set URL as a string
  - And then update the parser code to only check the URL,
    but save a string

## Weekend Plans

...I'm going to try again to go to Gamestop,
   and stand up to the bullies.

Buy a switch (the bullies say Real gamers don't play switches)

I've got new techniques:
  - Sticks and Stones will Break my Bones but words will Never hurt
  - I'm Rubber and your Glue, whatever say, bounces off me and
    stick to you.

## TODO

- SFX scavenger hunt
- Fix our Economy

## SFX scavenger hunt

- I have a bucket in Linode
  - Every 15 mins I will generate credentials (only good good 14 mins),
    that allow you to read in said bucket
  - I am going to place a bunch of junk in the bucket
  - And some sort of Gold Ticket (a sound that if you play),
    you win something

What is the Junk:
  -> background Photos
  -> I need a script to upload all my photos

Where is the ticket:
  -> What will the ticket look like?
  -> do I just make a text file, with a soundname maybe

## Resources

https://www.ebay.com/itm/Vintage-Honeywell-Computer-Terminal-w-Hall-Effect-Keyboard-4B3E-switches/283938174328

carlvandergeest: I recommend tp-link, they are nice and cheap and have pro features

https://chatterbot.readthedocs.io/en/stable/

https://micro.mu/introduction

## Help Get My New Laptop

- System76
- Elitebook?

New Trend: Heavy Laptops are Cool Laptops

artmattdank: this (incredible cool guy) guy used to take this huge laptop to my art history class and it had its own wheeled suitcase and it was so huge and made so much noise when he was transporting it

## Upside of Apple

- Relaxing:
  - Open My Laptop, hit Xcode
    - Go make some coffee
    - Come back up my program
    - Run it
    - Take a Shower
    - Boot up Docker Compose
    - Go for a nice Walk

## Viewer Questions


endofunctorzero: programming shops or whatever is ecomerce in my mind

Are you tackling scaling problems?
   - That can be immensely interesting

## Begin's Suggestions

- Don't talk to babies in some wierd voice.
 I always talk to babies, the same way I talk to talk adults

 .....I don't say their names


## View Questions

justshutitdown: I want to learn to be more like @beginbot. How does one go about this?

beginbot:  .... write down all the things you like to do
write down all the things you don't like to do
Find what things you Don't like to do that, that you want to do
And Just start liking those things

Brainwash Yourself:

- Ohh its the best
- Everyone knows its the best

If you submit a sound with on  name, its set as your theme
it plays every day when you join
only you can play

it you can add other sounds, and you become the first
owner, and then others can fix

## Resources

- Thank you ssyuni, very useful
- https://medium.com/@felipedutratine/how-to-organize-the-go-struct-in-order-to-save-memory-c78afcf59ec2

## To Keep Yourself Humble

At least once a week, write some SQL by hand
Wear some Burlap while you do it
eat some unflavored oats
