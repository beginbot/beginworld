# Playing the Game

## Levels

Level 1:
  -> Collect Coal
  -> Collect Iron Ore
  -> Collect Stone
Level 2
  -> Add Markers and Teleport between each on
Level 3
  -> Make Iron Plates
Level 4
  -> Then inserters and Belts


## Step 1: Mine some resources

```
!n
!s
!e
!e
!stop
!mine
```

## Step 2: Convert iron-ore into iron-plates

```
!build stone-furnace
!build stone-furnace

!build stone-furnace
!build sf

!put coal
!put iron-ore
```

## Step 3: Create Electricity

- Find Water
- Build Offshore Pump
- Build Offshore Pump

!craft offshort-pump
!craft op
!craft boiler
!craft steam-engine
!craft se

-----------------------------------------------------------------------------------------

!craft iron-chest
!craft burner-inserter
!craft burner-inserter
!craft burner-mining-drill
!craft stone-furnace
!craft iron-gear-wheel


!build iron-chest
!build burner-inserter
!build burner-inserter
!build burner-mining-drill
!build stone-furnace
!build iron-gear-wheel

!mine
!~

!iron_ore
!iron
!coal
!take
