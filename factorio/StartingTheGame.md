# StartingTheGame

## Steps

- Start the Game Headless
- Connect to the Game through Steam
- Turn on IRC -> RCON Bot

## Concrete

```
 factorio \
  --start-server ~/.factorio/saves/begin4.1.2.zip \
  --rcon-port 27015                                    \
  --rcon-password password                             \
  -c ~/.factorio/config/server_config.ini              \
  --mod-directory ~/.factorio/mods

 factorio --create ~/.factorio/saves/begin4.1.3.zip \
  --start-server ~/.factorio/saves/begin4.1.3.zip \
  --rcon-port 27015                                    \
  --rcon-password password                             \
  -c ~/.factorio/config/server_config.ini              \
  --mod-directory ~/.factorio/mods

 factorio --start-server --create ~/.factorio/saves/begin4.1.3.zip \
  --rcon-port 27015                                    \
  --rcon-password password                             \
  -c ~/.factorio/config/server_config.ini              \
  --mod-directory ~/.factorio/mods

 factorio --start-server ~/.factorio/saves/begin3.zip \
  --rcon-port 27015                                    \
  --rcon-password password                             \
  -c ~/.factorio/config/server_config.ini              \
  --mod-directory ~/.factorio/mods

 factorio --start-server ~/.factorio/saves/begin2.zip \
  --rcon-port 27015                                    \
  --rcon-password password                             \
  -c ~/.factorio/config/server_config.ini              \
  --mod-directory ~/.factorio/mods

factorio --start-server ~/.factorio/saves/begin.zip \
  --rcon-port 27015                                    \
  --rcon-password password                             \
  -c ~/.factorio/config/server_config.ini              \
  --mod-directory ~/.factorio/mods

factorio --start-server ~/.factorio/saves/begin.zip \
  --rcon-port 27015                                    \
  --rcon-password password                             \
  -c ~/.factorio/config/server_config.ini              \
  --mod-directory ~/.factorio/mods

factorio --start-server-load-latest --rcon-port 27015 --rcon-password password

factorio --start-server-load-latest ~/.factorio/saves/begin.zip --rcon-port 27015 --rcon-password password -c ~/.factorio/config/server_config.ini


LAST NIGHT
 factorio --start-server ~/.factorio/saves/begin.zip \
  --rcon-port 27015                                    \
  --rcon-password password                             \
  -c ~/.factorio/config/server_config.ini              \
  --mod-directory ~/.factorio/mods

 factorio --start-server ~/.factorio/saves/amoung.zip \
  --rcon-port 27015                                    \
  --rcon-password password                             \
  -c ~/.factorio/config/server_config.ini              \
  --mod-directory ~/.factorio/mods
```

   0.000 Error Util.cpp:83: Error configuring paths: There is no package core in /usr/share/factorio/server. Deduced executable directory: /home/begin/games/factorio/bin/x64, read data: __PATH__system-read-data__/server, write data: __PATH__system-write-data__/server

bin/x64/factorio --start-server ~/.factorio/saves/begin.zip \
   --server-settings /home/gamemaster/factorio/data/server-settings.json --start-server-load-latest

factorio --start-server-log-latest ~/.factorio/saves/begin.zip

  --rcon-port 27015                                    \
  --rcon-password password                             \

  -c ~/.factorio/config/server_config.ini              \
  --mod-directory ~/.factorio/mods

  ## Fresh Game

  - Create New Game in Facotrio GUI
    -> Save and Quit
  - Start headless pointing to save file
  - Reconnect Multiplayer
