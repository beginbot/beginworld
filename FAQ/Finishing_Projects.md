# Finishing Projects

If you can't answer these questions, you won't finish.

## When is the deadline?
## What happens if you don't hit the deadline?

Tip: Take advantage of Parkisons Law

https://www.wikiwand.com/en/Parkinson%27s_law

work expands so as to fill the time available for its completion

You have a school project due in 2 weeks.
How can you finish the whole thing the night before?

....yet its impossible to finish in a night, a week early

## What happens if you don't hit the deadline?

- People make fun of you
  - how can you set up a system where people make fun of you?
- Some public presentation:
  - Meetup Talk
  - Twitch Stream
- Publish something
  - Blog Post
  - Youtube Video
