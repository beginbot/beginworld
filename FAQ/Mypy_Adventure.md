# MYPY Adenture

## Goal: Use MonkeyType to add Types to Chat Thief

## Learnings

The system profile function is called only on call and return,
but the return event is reported even when an exception has been set

The function is thread-specific, but there is no way for the profiler to know
about context switches between threads. Don't use with multiple

## Assumptions

This is clearing the tracing

```python
sys.settrace(None)
```

The same tracing function can be used by settrace or setprofile

Try using sys.setprofile instead of sys.settrace when tracing builtin functions:

## Open Questions

- What is the performance impact of settrace and/or setprofile

## Confusion

- [sys.setprofile](https://docs.python.org/3/library/sys.html#sys.setprofile)
Also, its return value is not used, so it can simply return None.
Error in the profile function will cause itself unset.

- I ran monkeytype run bot.py
  however it actually ran the bot!
  Not add types

## Things to Learn

- [sys.setprofile](https://docs.python.org/3/library/sys.html#sys.setprofile)
- [libcst](https://pypi.org/project/libcst/)

## Resources

- <https://www.programcreek.com/python/example/6537/sys.setprofile>
- <https://github.com/IBM/pyflowgraph/issues/13>
- <https://stackoverflow.com/questions/40674861/how-to-trace-builtin-functions-in-python>

## Other Things

[setrace](https://docs.python.org/3/library/sys.html#sys.settrace)
[django-snoopy](https://pypi.org/project/django-snoopy/)

## Type Opinions

Previous -> All the "backend" was typed Python
  For a large code base -> Types are very valuable

  For Scripts -> Not sooo much

  Adding Types Later -> Annoying
