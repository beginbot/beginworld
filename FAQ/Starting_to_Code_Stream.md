# Advice for starting to Stream Code


zanuss: make streampope where I can have everything but all the sounds

Suggestion for Python Project First DB for someone Learning:
  - Beginbot: Postgres (I could be convinced MySQL....but wuold prefer MariaDB)
  - Teej: Sqlite
  - Erik: Postgres or Mysql
      - erikdotdev: I see a lot of beginners using MongoDB, which i kind of have feels about.

## 0 Viewers

- Just treat the first 6 months of coding on Twitch,
as this beautiful nice study time.
....soon those days of learning and productive will be over,
once the viewers show up.

ALWAYS HIDE ALL VIEWER NUMBERSSSSSS
ALL VIEWER NUMBERS

### Getting Started

Begin opinions: I think its better to do more smaller streams
at first.

It's hard to press go live, than it is to stay live.

Often theres a tendency to want to do longer streams in the beginning
and then you build them up too much in your head between streams.

Goal just one 1 Hour
Move up 1-2 Hours as its fun
Soon we hit 4 hours just naturally because it's fun
and we don't want to stop

Every time you go live and do an hour stream, you probably
have a 5 more things on your TODO list.
And often short stream, doing those TODOs, going live again,
finding the next list of TODOs, is a better stream
improvement way.

## Programming Specific

- make ya text bigger
  - people can't read, this won't be the same setup as your regular coding

- Talk through what you doing the whole time. Never shut up.
  pretend rubber ducking with a friend

- In you title, make the first words, the tech
  python, react, javascript, opensource, Redis, AWS

- Take notes and share with the users, make a repo, take notes,
  share them reflect.
  - Write down some todos each stream, and just fix a couple things.

- People are going to constantly ask your stack, you should
 have it in your bio, and chatbot command
 Editor, OS, Language, Library/Framework, Job, Keyboard,
 Terminal Emulator, Terminal Mutliplexer

- You don't have anyone interact in the beginning, and its confusing
  coming to a code stream. Don't be afraid to repeat what you
  are working over and over again. One you better at, you figure out
  how to automate it, soon your chatters do it for you.

## Generic Streaming Advice

- How can you respond to people fast
  - You need the chat up
  - you need to respond fast

- Don't worry about the visuals
  - overlays
  - fancy stuff
  - who cares if the framerate is low and resolution is 720 or lower
  - Don't go crazy with the bitrate

- Lots of things are going to go wrong
  - and its all good
  - Get them our of the way

## Everything Falling Apart

- in OBS you can set how many retries and how long to try between retries,
  when you lose connection. Max that shit out.

- When I was streaming on the Mac, and hooked up OBS
  Websockets to Python:
  - We got 7 crashes in a single stream.k

## Darkside

You stream in silence alone for a while.
I did 3 months, no-one
one person pops by
they come every once in a while

Another does
soon you have a mesh of people to give you a little
base, then it grows
