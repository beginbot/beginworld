# Why write your tests first

- Write your tests first, forces you to follow
a bunch of patterns, that make testing easier,
but incidentally make you code better

For Example:

- it it harder to test big ole complicated methods, with intense setup.
  you end up breaking them down  into more composable pieces.

- You have make all of your configuration, configurable in the beginning:
   - API key
   - DB set etc.
  - This helps get some requirements out of the way BEFORE you start designing

- Test in the beginning, and have a higher level list of tests to pass,
  you stay more focused on the task at hand.

