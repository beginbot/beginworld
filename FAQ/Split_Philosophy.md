# Split Philosophy

## Questions

- WHy a window manager AND A Terminal Multiplexer
  - building out "workspaces", reattaching to them and what not

Vim Splits
  -> when you're in one project, editing!!! editing!!!

Tmux Splits
  -> When you are running multiple projects, or editing one side,
    and running something on the other.

I3 Splits
  -> Separate Programs, or programs that launch other programs
