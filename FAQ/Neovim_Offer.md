# Neovim Offer

!dropeffect 200 moliendo

## Limited Time Offer

If you switch from Vim (or any editor) to NeoVim Today:
  (send a pic in the discord)

We'll give you the welcome package:
  - 200 SFX from Beginbot
  - Unlimited support from Neovim Core Team Member Teej
  - Primeagan will give you a discount on his on Mastering Vim Certificate

If You Act Now and Build From Source, we'll include Prime, Vim and Teej Soundpacks!
