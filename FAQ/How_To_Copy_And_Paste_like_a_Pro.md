# How To Copy And Paste like a Pro

- Copy the thing over
- Retype the whole thing
- Write down everything you are confused on, or don't understand
- Research said thing
- Keep researching and playing with, until you understand,
  why it's written the way it is


## The Problems with this Technique

- When to Stop Rabbit Holing

## What makes a Senior Versus A Junior

- Seniors know when to stop going down the rabbit hole

As a junior, you have to develop this sense.
However, its ok to fall all the way down in the beginning.

## Young Begin Planned Rabbit Hole Spirals

- Pick a day, and never stop going down the rabbit hole,n
  and copy down every level you go down
