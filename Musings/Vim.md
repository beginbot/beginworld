## Habit Changing

- Make it impossible
  - disable you arrow keys
- Make it annoying
  - put a sleep

## 5 Years Ago Begin

I sat down in a quiet room, and thought to myself, what do I do the most in Vim.
Saving!
.......how can I make this the easiest???
at the time, on my mac keyboard, my ring finger rests on k, above, `,,`
so I made my comma

....Past Begin this made this up, make your own things up!

## What I am Missing

- What are home and end for??

## Vim Day Ponderings

- The Art of Remapping:
  - The Dangers of Remapping in insert mode
    - jk, or the reverse kj
    - Will this pattern occur naturally
    - What happens if does occur naturally
    - Whats yo timeout

0_nerd: I just did `imap ;; <C-S>b` FeelsGoodMan
0_nerd: My timeout is really short.

Will ;; appear in my regular work.
Where does ;; exist in the world

;; in bash, could be a problem for this remapping.

## Who ends up on Layer 0

- How do you handle you paired friends on Layer-0
  - Programmers are obsessed with: {} () []

## Vim Feelings

code_bleu: vim or nvim...that is the question. I get that nvim has all the cool
stuff, but wouldn't you trap yourself in a corner if you have to ssh into 100's
of servers and more than likely they wont have nvim.

Going from your customized vim, to server vi/vim is the same as going from
neovim to vanilla vi/vim

BEGINBOT: I HAVE SOOO MUCH TO SAY. THIS IS ME

## The Vim Help Style


- From talkign to Teej
  - All the details
  - .....then an example

## Local Leaders

you could remap leader, in your vimrc, and have different
keymaps that have different leader commands

Be care of where you define your leader key,
define it up top!!!!

Different Leader, per language
  - Then:

I don't need to decide what localleader is....I could just comma
....but then you got update

Beginbot is Wrong
> - File specifics mappings....but you don't want use leader.
>   sooo its li

- Leader VS Local Leader:
   - Mappings in Plugins!!!! Teej
    - nothgin diff for you personall...unless you are going to switch leaders.
      helpful for leader transition

if you release a plugin with key mappings using leader instead of localleader,
means you are Vim Criminal, Viminal


## Explore one day

Should I be working against one Make target (go exectutable),
and then switching the makeprg, or should I have alias
for select diff make targets

make
make web
make debug
make test

3 Programs:
  - Main the Chatbot
  - WebApp
  - Debug App for Testing

## Ponderings

- What are keys namespaces for yourself?
  - Example: p n> project wide

stupac62: @beginbot thoughts: (vim keybinds) you shouldn't use leader for insert mode keybinds.

What keys for Key Mapping:
  - What do map under CTRL?
  - What do you map under your leader?

## Jokes

  - CTRL-y and CTRL-e in insert mode, below or above respectively

## Researching

- rockerboo: https://github.com/tjdevries/express_line.nvim

- vf%d = one of my most used commands
- Prime

- How do we move lines up and down
- ddkP

## Mouseless Goals

- I don't think I'm going to throw my mouse away
- But i'm going to keep it off my desk,
  to see what other habits form
  ....lynx, QuteBrowser

## Ideas

Hello Holy Vim Lord, just bouncing an idea for a Vim Game based on todays confusion.

Vim Scavenger Hunt

Game where random things are placed in Vimland and you're told to fetch them.
  - Marks
  - Registers
  - Quickfix
  - Location List
  - Other places!?!?!

Then you can measure the keystrokes, and provide feedback, or tips.

Also I want like Zelda soundeffects for finding treasure, weapons and what not.

Think theres some meat there? or will it be too limited

## Improving Use of Help

My Help was broke, but now seems to work.
I need to make sure I am using help to its fullest.

- https://vim.fandom.com/wiki/Learn_to_use_help
- CTRL-] -> ref in help
  Press Ctrl-] to follow the link (jump to the quickref topic).
  After browsing the quickref topic, press Ctrl-T to go back to the previous topic.

## Scraps

" builtin.current_buffer_fuzzy_find = function(opts)
" -- Confert currently quickfixlist to telescope
" require('telescope.builtin').quickfix()
" -- Convert currently loclist to telescope
" require('telescope.builtin').loclist()

How do I get all the names in a folder

Works like a charm!
```
:r ls config/
```

## TODO

meatchops: bindkey '^e' edit-command-line in zsh to edit current command in vim, save and close to enter it

## Debugging

rockerboo: run nvim with nvim -V2 /tmp/nvim.log
rockerboo: then you can see what files its sourceing
teej: :scriptnamesu

## Python


Neovim Vim Nightly -> Builtin LSP
- Add PyMS Microsoft's Python language server
- hey completion and diagnostic, run, when you see the python language server
  running

## Neovim Advice

- Don't lie to Neovim
  - Specifically related to what your terminal can handle

## Teej Challenge

teej_dv: make your ^x^l thing optionally complete other windows
teej_dv: it's a good exercise
erikdotdev: @teej_dv no fuzzy finder for spelling? You're slipping.
teej_dv: we can probably make telescope finder for spell
teej_dv: :)

## Help ins-completion

Spelling: Ctrl-X CTRL-S

  - Philosophy
  - Physical
  - Indigenous
  - Confucianism
  - Zoroastrianism

Files:  CTRL-x CTRL-f | keep repeating to navigate in nested files
  - How do we get the this to work for :e

L -> Whole line

keywords in 'dictionary'				            |i_CTRL-X_CTRL-K|
keywords in 'thesaurus', thesaurus-style		|i_CTRL-X_CTRL-T|
keywords in the current file				        |i_CTRL-X_CTRL-N|
keywords in 'complete'				              |i_CTRL-N| |i_CTRL-P|
Vim command-line					                  |i_CTRL-X_CTRL-V|

## Plugins

- vim-surround
  - Need to use this more effectively
  - We added vim-repeat, which is real nice

- Vim Signature
  - Toggling Marks
    - alphabetically moving between marks
      - Left-hand -> 2nd layer ]
      - Right Hand ' with the pinky

- Telescope:
  - CTRL-v -> open in split (s is also an option....but who horizontal splits?)

### Keymaps

- <leader>tg -> telescoping Greping
- <leader>tp -> File searching
- <leader>vv -> vim dotfiles searching

### Open Questions:

- Who uses the arglist and for what
- How do people use their buffer list?
- When to use global versus local arglist
- Do we need errors in the file?
- What do errors look like?
- What does make mean in a vim context
- Whats the use difference for location list and quickfixlist
 - quickfixlist is project wide
  - location is window specific.
    - you might use both, with location, to focus on a
    more specialized area
  - :vimgrep, :grep, :helpgrep, :make
  - :lvimgrep, :lgrep, :lhelpgrep, :lmake

> From inside Vim an easy way to run a command and handle the output is with the
> |:make| command (see below).

```
If you have the error messages in a file you can start Vim with: >
	vim -q filename
```

The 'errorformat' option should be set to match the error messages from your
compiler (see |errorformat| below).

A location list is a window-local quickfix list. You get one after commands
like `:lvimgrep`, `:lgrep`, `:lhelpgrep`, `:lmake`, etc., which create a
location list instead of a quickfix list as the corresponding `:vimgrep`,
`:grep`, `:helpgrep`, `:make` do.
A location list is associated with a window and each window can have a
separate location list.  A location list can be associated with only one
window.  The location list is independent of the quickfix list.



When a window with a location list is split, the new window gets a copy of the
location list.  When there are no longer any references to a location list,
the location list is destroyed.

:mak[e][!] [arguments]	1. All relevant |QuickFixCmdPre| autocommands are
			   executed.

3. An errorfile name is made from 'makeef'.  If
'makeef' doesn't contain "##", and a file with this
name already exists, it is deleted.

When "##" is included, it is replaced by a number to make the name
unique.  This makes sure that the ":make" command doesn't overwrite an
existing file.

----

stupac62: @soulshined if you like math then you like ligatures. IMO
theprimeagen: so i like the idea of p as my first key for anything "project" wide (you can do a). That way I can do pw for grep for word under cursor. pW for WORD under cursor. ps for project search. etc etc
theprimeagen: I love mnemonic
dzintars_dev: !ligatures
teej_dv: newmonics are nice

---

- What if we have multiple targets to build
- How can I use other make targets, for the quickfixlist

## Completion Woes

- Different Key combos for triggering completion
- Different Keys for moving through results

- CTRL-X CTRL-S

## All The Lists

- Quickfix list, Location List, Argslist

## Viewer Questions

code_bleu: I have changed my <esc> to "kj", but can do "kj" when in "insert (Paste)" mode. Can i get the "kj" to work there too?

## Folder Structure

~/.local/share/nvim/site - ~/.config is for "configs" and plugins are downloaded blobs. so i.e. ~/.local/share/nvim/site/autoload or say ~/.local/share/nvim/site/plugged

:help stdpath

## Mysteries

hello_im_nix: Do you know how to disable the neovim floating window?
floaterm window
https://github.com/voldikss/vim-floaterm
:help floaterm-faq
mccannch: @Hello_im_nix why don't you want fzf, rg into a float term? I think I am confused

## Git

- Lazy Git
- Delta
- Vim-Fugative

## Highlight

teej_dv: ":help vim.highlight.on_yank()"
teej_dv: to see config options if you wanted begin, btw

### Too Many Dos

Also see |:windo|, |:tabdo|, |:bufdo|, |:cdo|, |:ldo|,
|:cfdo| and |:lfdo|.

!jdi

## Foolish Begin

- How do we handle renaming, moving around files better
  - Used to use Nerdtree
  - Using dirvish now (not that well)
  - Like using ranger for moving stuff around

### Desires

- How do I get  the quickfixlist to stay populated when closing the Buffer
- How do I populate my Quickfixlist, without jumping to an error?
  - Potential running make opening new buffers (hook it up to sound
    to punish myself)

- How do I run other make targets and have them populate the
  quickfixlist

  set makeprg=make\ web

- Deleting triple quotes
  - 3ds"
"hello nice"
"isidentical"

- Autospell correct mode
  - When I leave insert mode, auto-correct spelling on the line

## Increasing the respect of Vim

- Neoclassical Visual Editor Improved

drdoomenberg: Neo Classical Visual Editor on a Tablet based Input System?
stupac games: Join twitch chats, ask for advice, and then half through tell
them: BTW: I'm on Neo Classical Visual Editor on a Tablet based Input System?

## Emotions

- https://github.com/romkatv/powerlevel10k
- What is this all doing for us, what do we want to remove?
- What do we want to customize

## Annoyed with

- Surround quotes life. Too many auto-quotes
  - vim surround
  - its based on motions, so flexible
  - keymap for double quotes
  - https://github.com/tpope/vim-surround
- Making Markdown Links
  - Move to Lua??
- Goyo is breaking thangs on exit
- Auto complete for vim things
- Terraform LSP
  - https://github.com/tjdevries/config_manager/blob/master/xdg_config/nvim/after/plugin/z_langserver.vim
- indentation

## TODO

- Hunter
- https://github.com/nikvdp/neomux
- https://github.com/mfussenegger/nvim-dap

## Back and Forward in History

Ctrl-i -> forward
Ctrl-o -> backward
Ctrl-^ -> flip between two files
'' -> last jump

## New Shortcuts

- <leader>vv -> search in nvim dotfiles
- <leader>rr -> ranger in floatterm
- <leader>lg -> lazygit in floatterm

