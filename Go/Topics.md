# Go Topics

- Go Logging;
  - Structured Logging
  - JSON Logging
  - The Context package

- `GO111MODULE=on go get     gitlab.com/user/project@branch`
- https://dev.to/maelvls/why-is-go111module-everywhere-and-everything-about-go-modules-24k
